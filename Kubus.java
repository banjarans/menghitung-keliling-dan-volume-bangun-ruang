package com.company;

public class Kubus implements Operasi, Operasi2 {
    double s;

    @Override
    public double Keliling() {
        return 12 * s;
    }

    @Override
    public void Volume() {
        double hasil;
        hasil = s * s * s;
        System.out.print("Volume Kubus : " + hasil);
    }
}
