package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int p1, p2;
        Scanner in = new Scanner(System.in);
        Kubus kbs = new Kubus();
        Balok blk = new Balok();
        Tabung tbg = new Tabung();
        Kerucut krc = new Kerucut();
        Bola bol = new Bola();

        System.out.println("Pilih Jenis Bangun Ruang");
        System.out.println("1. Kubus");
        System.out.println("2. Balok ");
        System.out.println("3. Tabung");
        System.out.println("4. Kerucut");
        System.out.println("5. Bola");
        System.out.print("Masukkan pilihan : ");
        p1=in.nextInt();

        switch (p1){
            case 1:
                System.out.println("\n== Anda memilih Kubus ==");
                System.out.print("Masukkan sisi : ");
                kbs.s = in.nextDouble();
                System.out.println("1. Cari Keliling");
                System.out.println("2. Cari Volume");
                System.out.print("Masukkan Pilihan : ");
                p2 = in.nextInt();
                if(p2==1){
                    System.out.print("Keliling Kubus : " + kbs.Keliling());
                }else if (p2==2){
                    kbs.Volume();
                }
                break;
            case 2:
                System.out.println("\n== Anda memilih Balok ==");
                System.out.print("Masukkan Panjang : ");
                blk.p = in.nextDouble();
                System.out.print("Masukkan Lebar   : ");
                blk.l = in.nextDouble();
                System.out.print("Masukkan Tinggi  : ");
                blk.t = in.nextDouble();
                System.out.println("1. Cari Keliling");
                System.out.println("2. Cari Volume");
                System.out.print("Masukkan Pilihan : ");
                p2 = in.nextInt();
                if(p2==1){
                    System.out.print("Keliling Balok : " + blk.Keliling());
                }else if (p2==2){
                    blk.Volume();
                }
                break;
            case 3:
                System.out.println("\n== Anda memilih Tabung ==");
                System.out.println("\nMencari Volume Tabung");
                System.out.print("Masukkan jari   : ");
                tbg.r = in.nextDouble();
                System.out.print("Masukkan Tinggi : ");
                tbg.t = in.nextDouble();
                tbg.Volume();
                break;
            case 4:
                System.out.println("\n== Anda memilih Kerucut ==");
                System.out.println("\nMencari Volume Kerucut");
                System.out.print("Masukkan Jari   : ");
                krc.r = in.nextDouble();
                System.out.print("Masukkan Tinggi : ");
                krc.t = in.nextDouble();
                krc.Volume();
                break;
            case 5:
                System.out.println("\n== Anda memilih Bola ==");
                System.out.println("\nMencari Volume Bola");
                System.out.print("Masukkan Jari : ");
                bol.r = in.nextDouble();
                bol.Volume();
                break;
            default:
                System.out.println("Input salah");
        }
        System.out.println();
    }
}
