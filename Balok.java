package com.company;

public class Balok implements Operasi, Operasi2{
    double p, l, t;

    @Override
    public double Keliling() {
        return 4 * (p + l + t);
    }

    @Override
    public void Volume() {
        double hasil;
        hasil = p * l * t;
        System.out.print("Volume Balok : " + hasil);
    }
}
